package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.Credential;
import com.rhombus.domain.dal.repository.AbstractJpaRepository;
import com.rhombus.domain.dal.repository.CredentialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 12:17
 */

@Service
public class CredentialServiceImpl extends GenericServiceImpl<Credential> implements CredentialService {

	private final CredentialRepository repository;

	@Autowired
	public CredentialServiceImpl(CredentialRepository repository) {

		this.repository = repository;
	}

	@Override
	public Credential findCredentialByUsername(String username) {

		return repository.findCredentialByUsername(username);
	}

	@Override
	public AbstractJpaRepository<Credential> getRepository() {

		return repository;
	}
}
