package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.JpaModelBase;
import com.rhombus.domain.dal.repository.AbstractJpaRepository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.ValidationException;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 19/9/2020
 * Time: 00:08
 */

public abstract class JpaCrudServiceImpl<T extends JpaModelBase> implements JpaCrudService<T> {

	@Override
	public void delete(String id) {
		T item = get(id);
		if(item == null) {
			throw new EntityNotFoundException();
		}
		item.setUpdated(ZonedDateTime.now());
		item.setDeleted(ZonedDateTime.now());
		save(item);
	}

	@Override
	public boolean exists(T item) {

		return exists(item.getId());
	}

	@Override
	public boolean exists(String id) {

		if (StringUtils.isEmpty(id)) {
			return false;
		}

		return get(id) != null;
	}

	@Override
	public List<T> get() {

		return getRepository().findAll();
	}

	@Override
	public T get(String id) {

		return getRepository().findById(id).orElse(null);
	}

	@Override
	public T post(T item) {

		return save(item);
	}

	@Override
	public T put(T item) throws ValidationException {

		if(item.getDeleted() != null) {
			throw new ValidationException("");
		}
		item.setUpdated(ZonedDateTime.now());
		return save(item);
	}

	private T save(T item) {

		return getRepository().save(item);
	}


	@Override
	public abstract AbstractJpaRepository<T> getRepository();
}
