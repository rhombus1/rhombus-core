package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.Credential;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 12:16
 */

public interface CredentialService extends GenericService<Credential> {

	Credential findCredentialByUsername(String username);
}
