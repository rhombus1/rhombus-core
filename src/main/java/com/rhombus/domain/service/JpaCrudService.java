package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.JpaModelBase;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.xml.bind.ValidationException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 19/9/2020
 * Time: 00:06
 */

public interface JpaCrudService<T extends JpaModelBase> {
	void delete(String var1);

	boolean exists(T item);

	boolean exists(String id);

	List<T> get();

	T get(String id);

	T post(T item);

	T put(T item) throws ValidationException;

	PagingAndSortingRepository<T, String> getRepository();
}
