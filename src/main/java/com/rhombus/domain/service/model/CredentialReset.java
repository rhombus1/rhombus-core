package com.rhombus.domain.service.model;

/**
 * Created by fmpanje on 8/9/2020
 */

public class CredentialReset {

	private String clientId;
	private String newEncryptedPassword;
	private String password;
	private String passwordResetToken;
	private String responseCode;
	private String responseDescription;
	private String username;

	public String getClientId() {

		return clientId;
	}

	public void setClientId(String clientId) {

		this.clientId = clientId;
	}

	public String getNewEncryptedPassword() {

		return newEncryptedPassword;
	}

	public void setNewEncryptedPassword(String newEncryptedPassword) {

		this.newEncryptedPassword = newEncryptedPassword;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getPasswordResetToken() {

		return passwordResetToken;
	}

	public void setPasswordResetToken(String passwordResetToken) {

		this.passwordResetToken = passwordResetToken;
	}

	public String getResponseCode() {

		return responseCode;
	}

	public void setResponseCode(String responseCode) {

		this.responseCode = responseCode;
	}

	public String getResponseDescription() {

		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {

		this.responseDescription = responseDescription;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}
}
