package com.rhombus.domain.service.model;

import java.util.Map;

/**
 * Created by fmpanje on 7/7/2020
 */

public class Registration {

	private Map<String, Object> additionalData;
	private String              city;
	private String              clientId;
	private String              country;
	private String              email;
	private String              encryptedPassword;
	private String              firstName;
	private String              lastName;
	private String              locality;
	private String              responseCode;
	private String              responseDescription;
	private String              telephone;
	private String              username;

	public Map<String, Object> getAdditionalData() {

		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {

		this.additionalData = additionalData;
	}

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getClientId() {

		return clientId;
	}

	public void setClientId(String clientId) {

		this.clientId = clientId;
	}

	public String getCountry() {

		return country;
	}

	public void setCountry(String country) {

		this.country = country;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getEncryptedPassword() {

		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {

		this.encryptedPassword = encryptedPassword;
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public String getLocality() {

		return locality;
	}

	public void setLocality(String locality) {

		this.locality = locality;
	}

	public String getResponseCode() {

		return responseCode;
	}

	public void setResponseCode(String responseCode) {

		this.responseCode = responseCode;
	}

	public String getResponseDescription() {

		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {

		this.responseDescription = responseDescription;
	}

	public String getTelephone() {

		return telephone;
	}

	public void setTelephone(String telephone) {

		this.telephone = telephone;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}
}
