package com.rhombus.domain.service.model;

import com.rhombus.domain.dal.entity.User;

import java.util.Map;

/**
 * Created by fmpanje on 7/7/2020
 */

public class Login {

	private Map<String, Object> additionalData;
	private Boolean             attemptsExceeded;
	/**
	 * Represents the unique client this user is using to login. Each client should send this @clientId to
	 * login successfully
	 */
	private String  clientId;
	/**
	 * The encrypted value of the password entered by the actor, (encrypted under a aes encryption key)
	 */
	private String  encryptedPassword;
	private Integer remainingAttempts;
	private String  responseCode;
	private String  responseDescription;
	private User    user;
	private Boolean userLocked;
	/**
	 * The username of the actor attempting authenticate.
	 */
	private String  username;

	public Map<String, Object> getAdditionalData() {

		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {

		this.additionalData = additionalData;
	}

	public Boolean getAttemptsExceeded() {

		return attemptsExceeded;
	}

	public void setAttemptsExceeded(Boolean attemptsExceeded) {

		this.attemptsExceeded = attemptsExceeded;
	}

	public String getClientId() {

		return clientId;
	}

	public void setClientId(String clientId) {

		this.clientId = clientId;
	}

	public String getEncryptedPassword() {

		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {

		this.encryptedPassword = encryptedPassword;
	}

	public Integer getRemainingAttempts() {

		return remainingAttempts;
	}

	public void setRemainingAttempts(Integer remainingAttempts) {

		this.remainingAttempts = remainingAttempts;
	}

	public String getResponseCode() {

		return responseCode;
	}

	public void setResponseCode(String responseCode) {

		this.responseCode = responseCode;
	}

	public String getResponseDescription() {

		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {

		this.responseDescription = responseDescription;
	}

	public User getUser() {

		return user;
	}

	public void setUser(User user) {

		this.user = user;
	}

	public Boolean getUserLocked() {

		return userLocked;
	}

	public void setUserLocked(Boolean userLocked) {

		this.userLocked = userLocked;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}
}
