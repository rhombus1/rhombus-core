package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.JpaModelBase;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by fmpanje on 6/7/2020
 */

public interface GenericService<T extends JpaModelBase> {

	void delete(String var1);

	boolean exists(T item);

	boolean exists(String id);

	List<T> get();

	T get(String id);

	T post(T item);

	T put(T item);

	PagingAndSortingRepository<T, String> getRepository();
}
