package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.User;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 12:19
 */

public interface UserService extends GenericService<User> {

}
