package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.JpaModelBase;
import com.rhombus.domain.dal.repository.AbstractJpaRepository;
import com.rhombus.exception.ValidationException;
import sun.reflect.generics.repository.AbstractRepository;

import javax.persistence.EntityNotFoundException;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by fmpanje on 7/7/2020
 */

public abstract class GenericServiceImpl<T extends JpaModelBase> implements GenericService<T> {

	@Override
	public void delete(String id) {
		T item = get(id);
		if(item == null) {
			throw new EntityNotFoundException();
		}
		item.setDeleted(ZonedDateTime.now());
		getRepository().save(item);
	}

	@Override
	public boolean exists(T item) {

		if(item == null) {
			return false;
		}
		return exists(item.getId());
	}

	@Override
	public boolean exists(String id) {

		if(id == null) {
			return false;
		}
		return get(id) != null;
	}

	@Override
	public List<T> get() {

		return getRepository().findAll();
	}

	@Override
	public T get(String id) {

		return getRepository().findById(id).orElse(null);
	}

	@Override
	public T post(T item) {

		if(item.getCreated() == null) {
			item.setCreated(ZonedDateTime.now());
		}
		item.setUpdated(ZonedDateTime.now());
		return getRepository().save(item);
	}

	@Override
	public T put(T item) {

		if(item.getId() == null) {
			throw new ValidationException("Entity not found");
		}

		if (item.getDeleted() != null) {
			throw new ValidationException(String.format("Entity with id %s was not found",item.getId()));
		}

		item.setUpdated(ZonedDateTime.now());
		return getRepository().save(item);
	}

	@Override
	public abstract AbstractJpaRepository<T> getRepository();
}
