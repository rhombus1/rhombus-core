package com.rhombus.domain.service;

import com.rhombus.domain.dal.entity.User;
import com.rhombus.domain.dal.repository.AbstractJpaRepository;
import com.rhombus.domain.dal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 12:19
 */

@Service
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {

	private final UserRepository repository;

	@Autowired
	public UserServiceImpl(UserRepository repository) {

		this.repository = repository;
	}

	@Override
	public AbstractJpaRepository<User> getRepository() {

		return repository;
	}
}
