package com.rhombus.domain.enums;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 19/9/2020
 * Time: 16:42
 */

public class ResponseCode {
	public static final String APPROVED           = "000";
	public static final String ATTEMPTS_EXCEEDED  = "102";
	public static final String DUPLICATE          = "026";
	public static final String ERROR              = "006";
	public static final String FORMAT_ERROR       = "030";
	public static final String INVALID_CREDENTIAL = "055";
	public static final String INVALID_TOKEN      = "103";
	public static final String NOT_FOUND          = "025";
	public static final String NO_CUSTOMER_RECORD = "048";
	public static final String SYSTEM_MALFUNCTION = "096";
	public static final String USER_LOCKED        = "104";
	public static final String VALIDATION_FAILURE = "101";
}
