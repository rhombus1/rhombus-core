package com.rhombus.domain.dal.entity;

import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by fmpanje on 7/7/2020
 */

@Entity
@Where(clause = "deleted is null")
@Table(name = "contact_details")
public class ContactDetail extends JpaModelBase {

	private String city;
	private String country;
	private String email;
	private String locality;
	private String telephone;

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getCountry() {

		return country;
	}

	public void setCountry(String country) {

		this.country = country;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getLocality() {

		return locality;
	}

	public void setLocality(String locality) {

		this.locality = locality;
	}

	public String getTelephone() {

		return telephone;
	}

	public void setTelephone(String telephone) {

		this.telephone = telephone;
	}
}
