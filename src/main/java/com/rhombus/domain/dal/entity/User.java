package com.rhombus.domain.dal.entity;

import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;

/**
 * Created by fmpanje on 6/7/2020
 */

@Entity
@Table(name = "users")
@Where(clause = "deleted is null")
public class User extends JpaModelBase {

	private String        bio;
	@JoinColumn(name = "contact_detail_id", referencedColumnName = "id")
	@OneToOne
	@Where(clause = "deleted is null")
	private ContactDetail contactDetail;
	private String        firstName;
	private ZonedDateTime lastLogin;
	private String        lastName;
	private String        notificationToken;
	private String        photoUrl;
	private long          rating;

	public String getBio() {

		return bio;
	}

	public void setBio(String bio) {

		this.bio = bio;
	}

	public ContactDetail getContactDetail() {

		return contactDetail;
	}

	public void setContactDetail(ContactDetail contactDetail) {

		this.contactDetail = contactDetail;
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	public ZonedDateTime getLastLogin() {

		return lastLogin;
	}

	public void setLastLogin(ZonedDateTime lastLogin) {

		this.lastLogin = lastLogin;
	}

	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public String getNotificationToken() {

		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {

		this.notificationToken = notificationToken;
	}

	public String getPhotoUrl() {

		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {

		this.photoUrl = photoUrl;
	}

	public long getRating() {

		return rating;
	}

	public void setRating(long rating) {

		this.rating = rating;
	}
}
