package com.rhombus.domain.dal.entity;

import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by fmpanje on 7/7/2020
 */

@Entity
@Table(name = "credentials")
@Where(clause = "deleted is null")
public class Credential extends JpaModelBase {

	private boolean active;
	private int     attempts;
	private String  clientId;
	private boolean exceedsAttempts;
	private boolean locked;
	private String  password;
	private String  passwordResetToken;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User    user;
	private String  username;

	public int getAttempts() {

		return attempts;
	}

	public void setAttempts(int attempts) {

		this.attempts = attempts;
	}

	public String getClientId() {

		return clientId;
	}

	public void setClientId(String clientId) {

		this.clientId = clientId;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getPasswordResetToken() {

		return passwordResetToken;
	}

	public void setPasswordResetToken(String passwordResetToken) {

		this.passwordResetToken = passwordResetToken;
	}

	public User getUser() {

		return user;
	}

	public void setUser(User user) {

		this.user = user;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public boolean isActive() {

		return active;
	}

	public void setActive(boolean active) {

		this.active = active;
	}

	public boolean isExceedsAttempts() {

		return exceedsAttempts;
	}

	public void setExceedsAttempts(boolean exceedsAttempts) {

		this.exceedsAttempts = exceedsAttempts;
	}

	public boolean isLocked() {

		return locked;
	}

	public void setLocked(boolean locked) {

		this.locked = locked;
	}
}
