package com.rhombus.domain.dal.repository;

import com.rhombus.domain.dal.entity.ContactDetail;
import org.springframework.stereotype.Repository;

/**
 * Created by fmpanje on 7/7/2020
 */

@Repository
public interface ContactDetailRepository extends AbstractJpaRepository<ContactDetail> {

}
