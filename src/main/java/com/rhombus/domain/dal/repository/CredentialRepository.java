package com.rhombus.domain.dal.repository;

import com.rhombus.domain.dal.entity.Credential;
import org.springframework.stereotype.Repository;

/**
 * Created by fmpanje on 7/7/2020
 */

@Repository
public interface CredentialRepository extends AbstractJpaRepository<Credential> {

	Credential findCredentialByUsername(String username);
}
