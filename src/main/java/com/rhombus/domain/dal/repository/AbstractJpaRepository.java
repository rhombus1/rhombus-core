package com.rhombus.domain.dal.repository;

import com.rhombus.domain.dal.entity.JpaModelBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 19/9/2020
 * Time: 00:00
 */

@NoRepositoryBean
public interface AbstractJpaRepository<T extends JpaModelBase> extends JpaRepository<T, String>,
		JpaSpecificationExecutor<T> {

}
