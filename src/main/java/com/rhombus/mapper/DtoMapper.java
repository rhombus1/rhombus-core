package com.rhombus.mapper;

import com.rhombus.api.dto.authentication.*;
import com.rhombus.domain.dal.entity.ContactDetail;
import com.rhombus.domain.dal.entity.User;
import com.rhombus.domain.service.model.CredentialReset;
import com.rhombus.domain.service.model.Login;
import com.rhombus.domain.service.model.Registration;
import org.mapstruct.Mapper;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 11:51
 */

@Mapper(componentModel = "spring")
public abstract class DtoMapper {

	public abstract ContactDetail map(ContactDetailDto contactDetail);

	public abstract ContactDetailDto map(ContactDetail contactDetail);

	public abstract Login map(LoginDto login);

	public abstract LoginDto map(Login login);

	public abstract Registration map(RegistrationDto registration);

	public abstract RegistrationDto map(Registration registration);

	public abstract User map(UserDto user);

	public abstract UserDto map(User user);

	public abstract CredentialReset map(CredentialResetDto credentialResetDto);

	public abstract CredentialResetDto map(CredentialReset credentialReset);
}
