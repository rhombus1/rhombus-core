package com.rhombus.exception;

import com.rhombus.api.dto.ErrorDto;

/**
 * Created by fmpanje on 7/7/2020
 */

public class ServiceException extends RuntimeException {

	private ErrorDto error;
	private Integer  httpStatus;

	public ServiceException() {

	}

	public ServiceException(ErrorDto error) {

		super(error.getErrorMessage());
		this.error = error;
	}

	public ServiceException(ErrorDto error, Throwable cause) {

		super(error.getErrorMessage(), cause);
		this.error = error;
	}

	public ServiceException(String message, Throwable cause) {

		this(new ErrorDto(message, message), cause);
	}

	public ServiceException(ErrorDto error, Integer httpStatus) {

		super(String.format("(http: %s) %s", httpStatus, error.getErrorMessage()));
		this.error = error;
		this.httpStatus = httpStatus;
	}

	public ErrorDto getError() {

		return error;
	}

	public Integer getHttpStatus() {

		return httpStatus;
	}
}
