package com.rhombus.exception;

public class ValidationException extends RuntimeException {

	private String displayError;

	public ValidationException(String message) {

		super(message);
	}

	public ValidationException(String message, String displayError) {

		super(message);
		this.displayError = displayError;
	}

	public String getDisplayError() {

		return displayError;
	}
}
