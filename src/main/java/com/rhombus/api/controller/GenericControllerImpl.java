package com.rhombus.api.controller;

import com.rhombus.api.dto.DtoBase;
import com.rhombus.domain.dal.entity.JpaModelBase;
import com.rhombus.domain.service.GenericService;
import org.springframework.http.ResponseEntity;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fmpanje on 6/7/2020
 */

public abstract class GenericControllerImpl<A extends DtoBase, D extends JpaModelBase> implements GenericController<A> {

	@Override
	public ResponseEntity<A> delete(String id) {

		getService().delete(id);
		return ok();
	}

	@Override
	public ResponseEntity<List<A>> get() {

		List<D> items = getService().get();
		return ok(map(items));
	}

	@Override
	public ResponseEntity<A> get(String id) {

		D item = getService().get(id);
		return ok(map(item));
	}

	protected abstract GenericService<D> getService();

	protected ResponseEntity<List<A>> k(List<A> body) {

		return ResponseEntity.ok().body(body);
	}

	protected abstract D map(A item);

	protected abstract A map(D item);

	protected List<A> map(List<D> items) {
		if (items == null) {
			return null;
		}
		List<A> result = new LinkedList<>();

		if (!items.isEmpty()) {
			items.forEach(item -> result.add(map(item)));
		}
		return result;
	}

	protected ResponseEntity<A> ok(A body) {

		return ResponseEntity.ok().body(body);
	}

	protected ResponseEntity<A> ok() {

		return ResponseEntity.ok().build();
	}

	protected ResponseEntity<List<A>> ok(List<A> items) {

		return ResponseEntity.ok().body(items);
	}

	@Override
	public ResponseEntity<A> post(A item) {

		D res = getService().post(map(item));
		return ok(map(res));
	}

	@Override
	public ResponseEntity<A> put(String id, A item) {

		D res = getService().put(map(item));
		return ok(map(res));
	}
}
