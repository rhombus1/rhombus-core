package com.rhombus.api.controller;

import com.rhombus.api.dto.DtoBase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 26/9/2020
 * Time: 10:13
 */

public abstract interface GenericController<T extends DtoBase> {

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	ResponseEntity<T> delete(@PathVariable(value = "id") String id);

	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<T>> get();

	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	ResponseEntity<T> get(@PathVariable("id") String id);

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<T> post(@RequestBody T item);

	@RequestMapping(method = RequestMethod.PUT, value = "{id}")
	ResponseEntity<T> put(@PathVariable("id") String id, @RequestBody T item);
}
