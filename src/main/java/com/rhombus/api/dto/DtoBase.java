package com.rhombus.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.ZonedDateTime;

/**
 * Created by fmpanje on 14/10/2019
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DtoBase {

	private ZonedDateTime created;
	private ZonedDateTime deleted;
	private String        id;
	private ZonedDateTime updated;

	public ZonedDateTime getCreated() {

		return created;
	}

	public void setCreated(ZonedDateTime created) {

		this.created = created;
	}

	public ZonedDateTime getDeleted() {

		return deleted;
	}

	public void setDeleted(ZonedDateTime deleted) {

		this.deleted = deleted;
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}

	public ZonedDateTime getUpdated() {

		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {

		this.updated = updated;
	}
}
