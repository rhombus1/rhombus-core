package com.rhombus.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by fmpanje on 6/7/2020
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorDto {
	private String componentName;
	/**
	 * Client description of the error which has occurred, for display to the end user
	 */
	private String displayMessage;
	/**
	 * Technical description of the error which occurred. For troubleshooting purposes
	 */
	private String errorMessage;
	private String moduleName;

	public ErrorDto(String displayMessage, String errorMessage) {

		this.displayMessage = displayMessage;
		this.errorMessage = errorMessage;
	}

	public ErrorDto(Exception ex) {
		this.errorMessage = ex.getMessage();
	}

	public String getComponentName() {

		return componentName;
	}

	public void setComponentName(String componentName) {

		this.componentName = componentName;
	}

	public String getDisplayMessage() {

		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {

		this.displayMessage = displayMessage;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}

	public String getModuleName() {

		return moduleName;
	}

	public void setModuleName(String moduleName) {

		this.moduleName = moduleName;
	}
}
