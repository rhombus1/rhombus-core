package com.rhombus.api.dto.authentication;

/**
 * Created by fmpanje on 8/9/2020
 */

public class CustomerCheckDto {

	private boolean pendingPassword;
	private boolean registered;
	private String  responseCode;
	private String  responseDescription;
	private String  username;

	public String getResponseCode() {

		return responseCode;
	}

	public void setResponseCode(String responseCode) {

		this.responseCode = responseCode;
	}

	public String getResponseDescription() {

		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {

		this.responseDescription = responseDescription;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public boolean isPendingPassword() {

		return pendingPassword;
	}

	public void setPendingPassword(boolean pendingPassword) {

		this.pendingPassword = pendingPassword;
	}

	public boolean isRegistered() {

		return registered;
	}

	public void setRegistered(boolean registered) {

		this.registered = registered;
	}
}
