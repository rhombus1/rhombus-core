package com.rhombus.api.dto.authentication;

import com.rhombus.api.dto.DtoBase;

/**
 * Created by fmpanje on 6/7/2020
 */
public class ContactDetailDto extends DtoBase {

	private String city;
	private String country;
	private String email;
	private String locality;
	private String telephone;

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getCountry() {

		return country;
	}

	public void setCountry(String country) {

		this.country = country;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getLocality() {

		return locality;
	}

	public void setLocality(String locality) {

		this.locality = locality;
	}

	public String getTelephone() {

		return telephone;
	}

	public void setTelephone(String telephone) {

		this.telephone = telephone;
	}
}
