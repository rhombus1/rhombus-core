package com.rhombus.api.dto.authentication;

import com.rhombus.api.dto.DtoBase;

import java.time.ZonedDateTime;

public class UserDto extends DtoBase {

	private String           bio;
	private ContactDetailDto contactDetail;
	private String           firstName;
	private ZonedDateTime    lastLogin;
	private String           lastName;
	private String           notificationToken;
	private String           photoUrl;
	private long             rating;

	public String getBio() {

		return bio;
	}

	public void setBio(String bio) {

		this.bio = bio;
	}

	public ContactDetailDto getContactDetail() {

		return contactDetail;
	}

	public void setContactDetail(ContactDetailDto contactDetail) {

		this.contactDetail = contactDetail;
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	public ZonedDateTime getLastLogin() {

		return lastLogin;
	}

	public void setLastLogin(ZonedDateTime lastLogin) {

		this.lastLogin = lastLogin;
	}

	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public String getNotificationToken() {

		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {

		this.notificationToken = notificationToken;
	}

	public String getPhotoUrl() {

		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {

		this.photoUrl = photoUrl;
	}

	public long getRating() {

		return rating;
	}

	public void setRating(long rating) {

		this.rating = rating;
	}
}
