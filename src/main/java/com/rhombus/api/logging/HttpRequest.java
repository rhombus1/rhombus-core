package com.rhombus.api.logging;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fmpanje on 11/8/2020
 */

public class HttpRequest {
	private     String              url;
	private String              method;
	private Map<String, String> headers    = new HashMap<>();
	private Map<String, String> parameters = new HashMap<>();
	private String              body;
	private String              remoteHost;

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}

	public String getMethod() {

		return method;
	}

	public void setMethod(String method) {

		this.method = method;
	}

	public Map<String, String> getHeaders() {

		return headers;
	}

	public void setHeaders(Map<String, String> headers) {

		this.headers = headers;
	}

	public Map<String, String> getParameters() {

		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {

		this.parameters = parameters;
	}

	public String getBody() {

		return body;
	}

	public void setBody(String body) {

		this.body = body;
	}

	public String getRemoteHost() {

		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {

		this.remoteHost = remoteHost;
	}
}
