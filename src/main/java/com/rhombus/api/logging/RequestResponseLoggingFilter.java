package com.rhombus.api.logging;

import com.rhombus.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by fmpanje on 11/8/2020
 */

@Component
@Order(1)
public class RequestResponseLoggingFilter extends OncePerRequestFilter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());


	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
	                                FilterChain filterChain) throws ServletException, IOException {

		ContentCachingRequestWrapper cachedRequest = new ContentCachingRequestWrapper(request);
		HttpServletResponse cachedResponse = new ContentCachingResponseWrapper(response);

		ZonedDateTime start = ZonedDateTime.now(ZoneId.of("Africa/Harare"));
		HttpRequest requestData = LoggingUtil.getHttpRequest(cachedRequest);

		if (Util.isValidJsonString(requestData.getBody())) {
			String body = requestData.getBody();
			if (body.length() > 32000) {
				requestData.setBody("Truncated...");
			}
		}
		log.info("ODOS MESSAGE REQUEST (Inbound)\n{}", Util.toJson(requestData));


		filterChain.doFilter(cachedRequest, cachedResponse);


		ZonedDateTime end = ZonedDateTime.now(ZoneId.of("Africa/Harare"));
		long rtt = ChronoUnit.MILLIS.between(start, end);

		HttpResponse responseData = LoggingUtil.getHttpResponse(cachedResponse, rtt);

		if (Util.isValidJsonString(responseData.getBody())) {
			if (responseData.getBody().length() > 32000) {
				responseData.setBody("Truncated...");
			}
		}

		log.info("ODOS MESSAGE RESPONSE (Outbound)\n{}", Util.toJson(responseData));
	}
}
