package com.rhombus.api.logging;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fmpanje on 11/8/2020
 */

public class HttpResponse {
	private     String              body;
	private Map<String, String> headers = new HashMap<>();
	private long                serverHttpMethodRtt;
	private String              status;

	public String getBody() {

		return body;
	}

	public void setBody(String body) {

		this.body = body;
	}

	public Map<String, String> getHeaders() {

		return headers;
	}

	public void setHeaders(Map<String, String> headers) {

		this.headers = headers;
	}

	public long getServerHttpMethodRtt() {

		return serverHttpMethodRtt;
	}

	public void setServerHttpMethodRtt(long serverHttpMethodRtt) {

		this.serverHttpMethodRtt = serverHttpMethodRtt;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}
}
