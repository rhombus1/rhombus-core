package com.rhombus.api.logging;

import com.rhombus.utils.Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fmpanje on 11/8/2020
 */

public class LoggingUtil {

	private static Map<String, String> getHeaders(HttpServletRequest request) {

		Map<String, String> map = new HashMap<>();

		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}
		return map;

	}

	private static Map<String, String> getHeaders(HttpServletResponse response) {

		Map<String, String> map = new HashMap<>();

		Collection<String> headerNames = response.getHeaderNames();
		for (String key : headerNames) {
			String value = response.getHeader(key);
			map.put(key, value);
		}
		return map;
	}

	public static HttpRequest getHttpRequest(HttpServletRequest httpServletRequest) throws IOException {

		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setUrl(httpServletRequest.getRequestURL().toString());
		httpRequest.setMethod(httpServletRequest.getMethod());
		httpRequest.setHeaders(getHeaders(httpServletRequest));
		httpRequest.setParameters(getParameters(httpServletRequest));
		httpRequest.setRemoteHost(httpServletRequest.getRemoteHost());
		String body = getRequestData(httpServletRequest);
		if (Util.isValidJsonString(body)) {
			httpRequest.setBody(Util.formatJsonString(body));
		} else {
			httpRequest.setBody(body);
		}
		return httpRequest;
	}

	public static HttpResponse getHttpResponse(HttpServletResponse httpServletResponse, long rtt) throws IOException {

		HttpResponse httpResponse = new HttpResponse();
		httpResponse.setHeaders(getHeaders(httpServletResponse));
		httpResponse.setServerHttpMethodRtt(rtt);
		String body = getResponseData(httpServletResponse);
		if (Util.isValidJsonString(body)) {
			httpResponse.setBody(Util.formatJsonString(body));
		} else {
			httpResponse.setBody(body);
		}
		httpResponse.setStatus(String.valueOf(httpServletResponse.getStatus()));
		return httpResponse;
	}

	private static Map<String, String> getParameters(HttpServletRequest request) {

		Map<String, String> map = new HashMap<>();

		Enumeration parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String key = (String) parameterNames.nextElement();
			String value = request.getParameter(key);
			map.put(key, value);
		}
		return map;

	}

	private static String getRequestData(final HttpServletRequest request) throws IOException {

		String payload = null;
		ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);

		if (wrapper != null) {
			byte[] buf = wrapper.getContentAsByteArray();
			if (buf.length > 0) {
				payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
			}
		}
		return payload;
	}

	private static String getResponseData(final HttpServletResponse response) throws IOException {

		String payload = null;

		ContentCachingResponseWrapper wrapper =
				WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
		if (wrapper != null) {
			byte[] buf = wrapper.getContentAsByteArray();
			if (buf.length > 0) {
				String charset = String.valueOf(StandardCharsets.UTF_8);
				if (!StringUtils.isBlank(wrapper.getCharacterEncoding())) {
					charset = wrapper.getCharacterEncoding();
				}
				payload = new String(buf, 0, buf.length, charset);
				wrapper.copyBodyToResponse();
			}
		}
		return payload;
	}

}
