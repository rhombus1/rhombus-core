package com.rhombus.utils;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * Created by IntelliJ IDEA.
 * User: fmpanje
 * Date: 5/10/2020
 * Time: 12:32
 */

public class CryptoUtils {

	public static String decrypt(String key, String value) {

		String ivKey = key.substring(0, 16);
		try {
			IvParameterSpec iv = new IvParameterSpec(ivKey.getBytes(StandardCharsets.UTF_8));
			SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(value));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static String encrypt(String key, String value) {

		String initVector = key.substring(0, 16);
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
			SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {

		String password = "anything";
		String key = "0123456789ABCDEFFEDCBA9876543210";

		String encrypted = encrypt(key, password);
		System.out.println("Encrypted: " + encrypted);

		String decrypted = decrypt(key, encrypted);
		System.out.println("Decrypted: " + decrypted);
	}
}
