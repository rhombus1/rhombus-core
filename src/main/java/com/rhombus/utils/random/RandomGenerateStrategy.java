package com.rhombus.utils.random;

/**
 * Created by fmpanje on 8/9/2020
 */

public class RandomGenerateStrategy {
	public static final String NUMERIC = "NUMERIC";
	public static final String ALPHANUMERIC_UPPER = "ALPHANUMERIC_UPPER";
	public static final String ALPHANUMERIC_LOWER = "ALPHANUMERIC_LOWER";
	public static final String ALPHANUMERIC_UPPERLOWER = "ALPHANUMERIC_UPPERLOWER";
	public static final String ALPHA_UPPER = "ALPHA_UPPER";
	public static final String ALPHA_LOWER = "ALPHA_LOWER";
}
