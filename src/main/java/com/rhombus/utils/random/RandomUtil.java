package com.rhombus.utils.random;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by fmpanje on 8/9/2020
 */

public class RandomUtil {
	private static Object MUTEX = new Object();

	public static String generateRandomPassword(int length, long seed) {

		return generateRandomPassword(length, RandomGenerateStrategy.ALPHANUMERIC_UPPERLOWER, seed);
	}

	public static String generateRandomPassword(int length) {

		return generateRandomPassword(length, RandomGenerateStrategy.ALPHANUMERIC_UPPERLOWER, 0);
	}

	public static String generateRandomPassword(int length, String strategy) {

		return generateRandomPassword(length, strategy, 0);
	}

	public static String generateRandomPassword(int length, String strategy, long seed) {

		return generateSecureRandomPassword(length, strategy, seed);
	}

	private static String generateSecureRandomPassword(int length, String strategy, long seed) {

		String characterSpace;
		switch (strategy) {
			case RandomGenerateStrategy.NUMERIC:
				characterSpace = "1234567890";
				break;
			case RandomGenerateStrategy.ALPHA_LOWER:
				characterSpace = "abcdefghjklmnpqrstuvwxyz";
				break;
			case RandomGenerateStrategy.ALPHA_UPPER:
				characterSpace = "ABCDEFGHJKLMNPQRSTUVWXYZ";
				break;
			case RandomGenerateStrategy.ALPHANUMERIC_LOWER:
				characterSpace = "abcdefghjklmnpqrstuvwxyz1234567890";
				break;
			case RandomGenerateStrategy.ALPHANUMERIC_UPPER:
				characterSpace = "ABCDEFGHJKLMNPQRSTUVWXYZ1234567890";
				break;
			case RandomGenerateStrategy.ALPHANUMERIC_UPPERLOWER:
			default:
				characterSpace = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz1234567890";
				break;
		}

		StringBuilder builder = new StringBuilder();
		synchronized (MUTEX) {

			try {

				if (seed == 0) {
					seed = SecureRandom.getInstance("SHA1PRNG").nextLong();
				}
				SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
				secureRandom.setSeed(seed);
				for (int i = 0; i < length; i++) {
					int character = secureRandom.nextInt(characterSpace.length());
					builder.append(characterSpace.charAt(character));
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}

		return builder.toString();
	}
}
