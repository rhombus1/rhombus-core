package com.rhombus.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by fmpanje on 10/7/2020
 */

public class Util {

	public static final  String       DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	private static final Logger       log              = LoggerFactory.getLogger(Util.class);
	private static final ObjectMapper objectMapper;
	private static double earthRadius = 6371;// Approximate radius of the earth in kilometers

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
		objectMapper.setDateFormat(new SimpleDateFormat(DATE_TIME_FORMAT));
		objectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	public static String calculateLuhn(String number) {
		// Get the sum of all the digits, however we need to replace the value
		// of the first digit, and every other digit, with the same digit
		// multiplied by 2. If this multiplication yields a number greater
		// than 9, then add the two digits together to get a single digit
		// number.
		//
		// The digits we need to replace will be those in an even position for
		// card numbers whose length is an even number, or those is an odd
		// position for card numbers whose length is an odd number. This is
		// because the Luhn algorithm reverses the card number, and doubles
		// every other number starting from the second number from the last
		// position.
		try {
			int sum = 0;
			for (int i = 0; i < number.length(); i++) {

				// Get the digit at the current position.
				int digit = Integer.parseInt(number.substring(i, (i + 1)));

				if ((i % 2) == 0) {
					digit = digit * 2;
					if (digit > 9) {
						digit = (digit / 10) + (digit % 10);
					}
				}
				sum += digit;
			}

			// The check digit is the number required to make the sum a multiple of
			// 10.
			int mod = sum % 10;
			int modValue = ((mod == 0) ? 0 : 10 - mod);
			return String.valueOf(modValue);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Only numeric digits can be used to generate a Luhn", e);
		}
	}

	public static <T> T fromJson(String string, Class<T> type) {

		try {
			return fromJsonUnCaught(string, type);
		} catch (IOException e) {
			log.warn("Util Exception (fromJson)", e);
			return null;
		}
	}

	public static double getDistance(double lat1, double long1, double lat2, double long2) {
		double distance = Math.acos(Math.sin(lat2 * Math.PI / 180.0) * Math.sin(lat1 * Math.PI / 180.0) +
				Math.cos(lat2 * Math.PI / 180.0) * Math.cos(lat1 * Math.PI / 180.0) *
						Math.cos((long1 - long2) * Math.PI / 180.0)) * earthRadius;
		return distance;
	}

	public static double distance(double lat1,
	                              double lat2, double lon1,
	                              double lon2)
	{

		// The math module contains a function
		// named toRadians which converts from
		// degrees to radians.
		lon1 = Math.toRadians(lon1);
		lon2 = Math.toRadians(lon2);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		// Haversine formula
		double dlon = lon2 - lon1;
		double dlat = lat2 - lat1;
		double a = Math.pow(Math.sin(dlat / 2), 2)
				+ Math.cos(lat1) * Math.cos(lat2)
				* Math.pow(Math.sin(dlon / 2),2);

		double c = 2 * Math.asin(Math.sqrt(a));

		// Radius of earth in kilometers. Use 3956
		// for miles
		double r = 6371;

		// calculate the result
		return(c * r);
	}

	public static <T> List<T> fromJsonList(String value, Class<T> type) {

		try {
			return objectMapper.readValue(value,
					objectMapper.getTypeFactory().constructCollectionType(List.class, type));
		} catch (IOException e) {
			log.warn("Util Exception (fromJsonList)", e);
			return null;
		}
	}

	public static <T> T fromJsonMap(Map linkedHashMap, Class<T> type) {

		return objectMapper.convertValue(linkedHashMap, type);
	}

	public static <T> List<T> fromJsonMap(List<LinkedHashMap> linkedHashMaps, Class<T> type) {

		if (linkedHashMaps == null) {
			return new ArrayList<>();
		}

		LinkedList<T> convertedValues = new LinkedList<>();
		linkedHashMaps.forEach(t -> convertedValues.add(objectMapper.convertValue(t, type)));
		return convertedValues;
	}

	public static <T> T fromJsonMap(LinkedHashMap linkedHashMap, Class<T> type) {

		return objectMapper.convertValue(linkedHashMap, type);
	}

	public static <T> T fromJsonUnCaught(String string, Class<T> type) throws IOException {

		return objectMapper.readValue(string, type);
	}

	public static String toJson(Object object) {

		try {
			objectMapper.configure(SerializationFeature.WRAP_EXCEPTIONS, false);
			return objectMapper.writeValueAsString(object);
		} catch (JsonMappingException e) {
			log.warn("Util Exception (toJson) {}, enable TRACE logging for stack trace",
					e.getMessage());
			log.trace("Util Exception (toJson)", e);
			return null;
		} catch (Exception e) {
			log.warn("Util Exception (toJson)", e);
			return null;
		}
	}



	public static Map<String, Object> toJsonMap(Object object) {

		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

		return objectMapper.convertValue(object, Map.class);
	}

	public static boolean isValidJsonString(String jsonString) {

		try {
			Object json = objectMapper.readValue(jsonString, Object.class);
			if (json != null) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static String formatJsonString(String jsonString) throws IOException {

		if (!StringUtils.isBlank(jsonString)) {
			Object json = objectMapper.readValue(jsonString, Object.class);
			String formatted = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			return formatted;
		}
		return null;
	}

}
